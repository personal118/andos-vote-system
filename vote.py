
class Vote:

    def __init__(self, label, encrypted_label, encrypted_bulletin):
        self.label = label
        self.encrypted_label = encrypted_label
        self.encrypted_bulletin = encrypted_bulletin

    def __repr__(self):
        return f"M - {self.label}; E(M) - {self.encrypted_label}; E(B) - {self.encrypted_bulletin}"

