from utils import getLogger
from rsa import decrypt

class Seller:

    def __init__(self, labels, keys):
        """Инициализация продавца секретов 
        
        labels - массив n-битных секретов
        keys - массив содержащий индекс пары ключей, открытый и закрытый ключи RSA
            кол-во ключей должно быть t*(t-1), где t - количество покупателей
        """
        self.labels = labels
        self.keys = keys
        self.logger = getLogger("SELLER")

    def computeDecrypt(self, key_indexs, buyer_ys):
        """Вычисление обратной функции и xor с секретами 

        key_indexs - массив из индексов пары ключей RSA
        buyers_ys - массив из случайных кортежей покупателей
        """
        res = []
        for i, label in enumerate(self.labels):
            ans = label
            for j, buyer_y in enumerate(buyer_ys):
                ans = ans ^ decrypt(self.keys[key_indexs[j]][2], buyer_y[i])
            res.append(ans)
        
        self.logger.debug(f"Подсчет обратных функций и xor. Key Indexs - {key_indexs}; Ys - {buyer_ys} ")
        return res

    def __repr__(self):
        return f"{self.labels} - {self.keys}"