from utils import getLogger
from rsa import encrypt
from binary.utils import formatBin, setBit

class Buyer:

    def __init__(self, id, label_length, pubKeys = [], labels = []):
        """"Покупатель

        id - айди покупателя
        pubKeys - массив из индекса пары ключей RSA и открытого ключа RSA 
        labels - массив из случайных котрежей
        label_length - длина продаваемых секретов
        """
        self.id = id
        self.pubKeys = pubKeys
        self.labels = labels
        self.label_length = label_length
        self.label = None
        self.logger = getLogger(f"VOTER-{id}")

    def computeIFB(self, pubKey, label):
        """Вычисление ИФБ"""
        # if len(self.pubKeys) == 0:
        #     raise Exception("Empty pubkeys")

        # pubKey = self.pubKeys[0][1]
        x = label
        f_x = encrypt(pubKey, x)

        res_lenght = self.label_length + 1
        res = []
        for i, c in enumerate(formatBin(x^f_x, res_lenght)):
            if c == '0':
                res.append(res_lenght - 1 - i)
        self.logger.debug(f"Посчитал ИФБ {pubKey} - {label}")
        return res

    def computeY(self, labels, IFB):
        """Заменой всех битов, индекс которых не входит в множество ИФБC, на их дополнения"""
        # if len(self.labels) == 0:
        #     raise Exception("Empty labels")

        # labels = self.labels[0][:]
        res = []
        for label in labels:
            f_label = formatBin(label, self.label_length)
            new_label = label
            for i in set(range(max(IFB))).difference(IFB):
                if f_label[len(f_label) - 1 - i] == '1':
                    new_label = setBit(new_label, i, 0)
                else:
                    new_label = setBit(new_label, i, 1)
            res.append(new_label)
        self.logger.debug(f"Подсчет Y - ИФБ {IFB} - Котреж - {labels}")
        return res

    def popPubKeys(self):
        """Удалить первый ключ"""
        return self.pubKeys.pop(0)

    def popLabels(self):
        """Удалить первый случайный кортеж"""
        return self.labels.pop(0)

    def setLabel(self, label):
        """"Установить приобритенный секрет"""
        self.logger.debug(f"Приобритён секрет {label}")
        self.label = label

    def __repr__(self):
        return f"{self.id} - {self.pubKeys} - {self.labels} - {self.label}"