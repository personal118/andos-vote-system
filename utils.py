from random import getrandbits
import os
import errno
import logging

def generateLabel(length):
    """Генерация метки длины length"""
    label = getrandbits(length)
    return label

def generateLabelCollection(length, label_length):
    """Генерация коллекции меток в количестве length, где каждая метка длины label_length"""
    labels = set()
    while len(labels) < length:
        labels.add(generateLabel(label_length))
    return list(labels)

def getLogger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(u"%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(ch)

    return logger

def createHandler(path):
    mkdir_p(os.path.dirname(path))
    fh = logging.FileHandler(path, encoding="utf-8")
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter(u"%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    fh.setFormatter(formatter)

    return fh

def mkdir_p(path):
    """http://stackoverflow.com/a/600612/190597 (tzot)"""
    try:
        os.makedirs(path, exist_ok=True)  # Python>3.2
    except TypeError:
        try:
            os.makedirs(path)
        except OSError as exc: # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else: raise