from andos import Buyer
from vote import Vote
from rsa import encrypt

class Voter(Buyer):

    def __init__(self, id, label_length, pubKeys=[], labels=[]):
        super().__init__(id, label_length, pubKeys=pubKeys, labels=labels)
        self.pubKey = None
        self.privateKey = None
        self.vote = None

    def generateVote(self, candidate):
        self.logger.info(f"Шифрует {candidate}")
        self.vote = Vote(self.label, encrypt(self.pubKey, self.label), encrypt(self.pubKey, candidate))

    def __repr__(self):
        return super().__repr__() + f"pubKey: {self.pubKey}; privateKey: {self.privateKey}"

