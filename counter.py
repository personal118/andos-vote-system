from rsa.rsa import decrypt, encrypt
from andos import Seller
import copy

class Counter(Seller):

    def __init__(self, labels, keys):
        super().__init__(labels, keys)
        self.votes = []
        self.privateKeys = []
        self.results = []

    def addVote(self, vote):
        self.logger.info(f"Получил голос - {vote}")
        self.votes.append(vote)

    def addPrivateKey(self, label, privateKey):
        self.privateKeys.append((label, privateKey))

    def computeResults(self, bulletin):
        results = copy.deepcopy(bulletin)
        for res in results:
            res["count"] = 0

        for i, vote in enumerate(self.votes):

            privateKey = None
            for j, key in enumerate(self.privateKeys):
                if vote.label in self.labels:
                    if vote.label == key[0]:
                        self.logger.debug(f"{vote.label} - {key} - {decrypt(key[1], vote.encrypted_label)}")
                        self.logger.debug(f"{decrypt(key[1], vote.encrypted_bulletin)}")
                        if vote.label == decrypt(key[1], vote.encrypted_label):
                            privateKey = key[1]
                            break
                
            if privateKey is None:
                self.logger.info("Не найден приватный ключ")
                continue

            result = (vote.label, decrypt(privateKey, vote.encrypted_bulletin))
            self.logger.info(f"Дешифровка - {result}")

            change = False
            for candidate in results:
                if candidate["id"] == result[1]:
                    self.logger.info(f"Голос отдан за {candidate['candidate']}")
                    candidate["count"] += 1
                    change = True
            
            if not change:
                self.logger.info(f"В базе есть две одинаковые метки")

        return results

            

            


    