from random import randint, getrandbits, random
from utils import generateLabel, getLogger, generateLabelCollection, mkdir_p, createHandler
from rsa import generateKeypair, encrypt, decrypt
from binary.utils import formatBin, setBit
from andos import Seller 
from voter import Voter
from counter import Counter
from vote import Vote
import time

# Кол-во голосующих
COUNT_OF_BUYERS = 2
# Кол-во бит у генерируемой метки
LABEL_LENGHT = 64
# Коэфицент расширения кол-ва меток в зависимости от кол-ва голосующих
# чем больше коэф, тем меньше шанс коллизий меток у голосующих
LABELS_BUYERS_EXPANSION = 1
# Кол-во кандидатов для выбора
COUNT_OF_CANDIDATE = 5

start_time = time.strftime("%Y%m%d-%H%M%S")
modeling_path = f".\\dist\\{start_time}"
mkdir_p(modeling_path)

all_log_file = createHandler(modeling_path + "//all.log")
andos_vote_system = getLogger("ANDOS-VOTE-SYSTEM")
andos_vote_system.addHandler(all_log_file)
central_election_commission = getLogger("CENTREL-ELECTION-COMMISSION")
central_election_commission.addHandler(all_log_file)
andos_distribution_system = getLogger("ANDOS-DISTRIBUTION-SYSTEM")
andos_distribution_system.addHandler(all_log_file)

bulletin_id_collection = generateLabelCollection(COUNT_OF_CANDIDATE, LABEL_LENGHT-1)
bulletin = []
for i in range(COUNT_OF_CANDIDATE):
    bulletin.append({
        "id": bulletin_id_collection[i],
        "candidate": f"Candidates {i}"
    })

andos_vote_system.info(f"Генерируем {COUNT_OF_BUYERS} голосующих...")
buyers = []
for i in range(COUNT_OF_BUYERS):
    andos_vote_system.debug(f"Генерация голосующего: {i}")
    v = Voter(id=i, label_length=LABEL_LENGHT)
    votter_log_file = createHandler(modeling_path + f"//voter-{i}.log")
    v.logger.addHandler(all_log_file)
    v.logger.addHandler(votter_log_file)
    buyers.append(v)


central_election_commission.info("Публикация списка голосующих...")
central_election_commission.debug(",".join(list(map(lambda x: f"Голосующий [id - {x.id}]", buyers))))

labels_count = len(buyers) * LABELS_BUYERS_EXPANSION
central_election_commission.info(f"Генерация {labels_count} уникальных меток длины {LABEL_LENGHT} бит...")

labels = generateLabelCollection(labels_count, LABEL_LENGHT - 1)

keys_count = len(buyers) * (len(buyers) - 1)
andos_distribution_system.info(f"Начало распределений уникальных меток между голосующими...")
andos_distribution_system.info(f"Генерация {keys_count} односторонних функций...")

keys = []
for i in range(keys_count):
    andos_distribution_system.debug(f"Генерация {i}-ой односторонней функции...")
    key = generateKeypair(LABEL_LENGHT)
    keys.append((i, key[0], key[1]))

andos_distribution_system.info("Инициализация продавца секртов...")
S = Counter(labels, keys)
counter_log_file = createHandler(modeling_path + "//counter.log")
S.logger.addHandler(counter_log_file)
S.logger.addHandler(all_log_file)

andos_distribution_system.info("Распределение односторонних функци по покупателям...")
for i, buyer in enumerate(buyers):

    # Создание случайных кортежей покупателей
    random_labels_collection = []
    for _ in range(len(buyers) - 1):
        random_labels_collection.append(generateLabelCollection(len(S.labels), label_length=LABEL_LENGHT - 1))

    # Отправка односторонних функций
    pubKeys = list(map(lambda x: x[:2], S.keys[i*(COUNT_OF_BUYERS - 1):(i+1)*(COUNT_OF_BUYERS - 1)]))
    buyer.pubKeys = pubKeys
    buyer.labels = random_labels_collection

    S.logger.info(f"Передал покупателю {buyer.id} одностороннюю функции")
    buyer.logger.info(f"Получил {len(pubKeys)} функций от продавца")
    buyer.logger.info(f"Сгенирировал {len(random_labels_collection)} котрежей")

andos_distribution_system.info("Начало покупок")
# print(buyers)
for index_current_buyer, current_buyer in enumerate(buyers):
    target_index = randint(0, len(labels) - 1)
    current_buyer.logger.info(f'Приобритение метки под номером {target_index}...')


    y_list = []
    random_buyer_labels_collection = []
    for index_buyer, buyer in enumerate(buyers):
        if index_current_buyer != index_buyer:
            # Случайный котреж, который получил текущий покупатель он другого покупателя
            random_buyer_labels = buyer.labels.pop(0)
            random_buyer_labels_collection.append(random_buyer_labels)

            # Для каждого случайного кортежа используем индивидуальную функцию
            pubKey = current_buyer.pubKeys.pop(0)
            # print(pubKey[1])
            # print(random_buyer_labels)

            # Считаем ИФБ
            current_buyer.logger.debug(f"Получил котреж от покупателя BUYER-{buyer.id}")
            buyer_ifb = current_buyer.computeIFB(pubKey[1], random_buyer_labels[target_index])

            # Ведем список результатов подсчета для t-1 покупателей
            y_list.append((pubKey, buyer.computeY(random_buyer_labels, buyer_ifb)))

    key_indexs = list(map(lambda x: x[0][0], y_list))
    buyer_ys = list(map(lambda x: x[1], y_list))

    # Считаем обратную функцию
    product = S.computeDecrypt(key_indexs, buyer_ys)[target_index]
    # Получаем результат
    for random_buyer_labels in random_buyer_labels_collection:
        product = product ^ random_buyer_labels[target_index]

    current_buyer.setLabel(product) 

andos_vote_system.info("Начало голосования...")
for index, buyer in enumerate(buyers):
    pubKey, privateKey = generateKeypair(LABEL_LENGHT)
    buyer.pubKey = pubKey
    buyer.privateKey = privateKey
    buyer.logger.info("Создание пары ключей RSA")

    print(f"Избиратель {index + 1} сделайте свой выбор: ")
    for index, candidate in enumerate(bulletin):
        print(f"{index + 1}. {candidate['candidate']}")

    vote_input = int(input())
    while not (vote_input - 1) in range(len(bulletin)):
        print("Такой кандидат отсутвует в бюллетени. Введите корректный номер: ")
        vote_input = int(input())
    selected_candidate_index = vote_input -1
    buyer.logger.info(f"Решает голосовать за {selected_candidate_index} - {bulletin[selected_candidate_index]['candidate']}")

    buyer.generateVote(bulletin[selected_candidate_index]["id"])

    second_label = S.addVote(buyer.vote)
    buyer.vote.second_label = second_label

andos_vote_system.info("Завершение голосования...")
for index, buyer in enumerate(buyers):
    S.logger.info(f"Получает закрытый ключ - {buyer.privateKey} для метки: {buyer.label} ")
    S.addPrivateKey(buyer.label, buyer.privateKey)

andos_vote_system.info("Подсчет голсов...")
res = S.computeResults(bulletin)

voice_count = 0
for c in res:
    voice_count += c["count"]

if voice_count != len(S.votes):
    S.logger.warning("Кол-во голосов и голосующих не совпадает")

S.logger.info(f"Результаты голсоовния:")
# res_str = ""
for index, candidate in enumerate(res):
    S.logger.info(f"{index+1}. {candidate['candidate']} набрал {candidate['count']} голосов")

    


